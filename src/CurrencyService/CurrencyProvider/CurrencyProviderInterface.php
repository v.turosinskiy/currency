<?php

namespace App\CurrencyService\CurrencyProvider;

/**
 * Interface CurrencyProviderInterface интерфейс для все провайдеров
 */
interface CurrencyProviderInterface
{
    /**
     * @return string возвращает имя провайдера
     */
    public function getName(): string;

    /**
     * возвращает значение валюты
     */
    public function getCurrency(string $name): float;

    /**
     * Обновляет значение валюты
     */
    public function setCurrency(string $name, float $value): CurrencyProviderInterface;

    /**
     * Возвращает следующий провайдер
     */
    public function getNext(): ?CurrencyProviderInterface;
}