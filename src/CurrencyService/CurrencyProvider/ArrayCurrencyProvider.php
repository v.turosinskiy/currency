<?php

namespace App\CurrencyService\CurrencyProvider;

/**
 * Class ArrayCurrencyProvider пример реализации когда хранилищем курсов валют служит массив
 */
class ArrayCurrencyProvider implements CurrencyProviderInterface
{
    /**
     * @var array
     */
    private $currencies = [];

    /**
     * @var string
     */
    private $name;

    /**
     * @var ArrayCurrencyProvider
     */
    private $nextProvider;

    public function __construct(string $name, array $currencies, ArrayCurrencyProvider $nextProvider = null)
    {
        $this->name = $name;
        $this->currencies = $currencies;
        $this->nextProvider = $nextProvider;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setCurrency(string $name, float $value): CurrencyProviderInterface
    {
        $this->currencies[$name] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getNext(): ?CurrencyProviderInterface
    {
        return $this->nextProvider;
    }

    /**
     * @inheritDoc
     */
    public function hasCurrency(string $name): bool
    {
        return isset($this->currencies[$name]);
    }

    /**
     * @inheritDoc
     */
    public function getCurrency(string $name): float
    {
        //весь этот метод улетит в CurrencyService
        //тут останется return $this->currencies[$name];
        $currency = null;
        if (!$this->hasCurrency($name)) {
            $nextProvider = $this->getNext();
            if ($nextProvider instanceof ArrayCurrencyProvider) {
                $currency = $nextProvider->getCurrency($name);
            } else {
                //тот случай когда ни один провайдер не вернул данные
                //стоит завести отдельный класс для ошибки
                throw new \Exception('ни один из провайдеров не вернул курс валюты');
            }
            // здесь стоит добавить обработку через событие
            $this->setCurrency($name, $currency);
        } else {
            $currency = $this->currencies[$name];
        }

        return $currency;
    }
}