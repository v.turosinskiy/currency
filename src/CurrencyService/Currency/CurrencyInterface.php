<?php

namespace App\CurrencyService\Currency;

interface CurrencyInterface
{
    public const RU = 'RU';
    public const EUR = 'EUR';
    public const US = 'US';
}